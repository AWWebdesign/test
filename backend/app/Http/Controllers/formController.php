<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FormController extends Controller
{
    public function __construct()
    {
    }


    public function submit(Request $request)
    {
        if (!isset($request) || empty($request)) {
            return;
        }

        $currentTime = date('Y-m-d H:i:s');

        foreach ($request->toArray() as $key => $value) {
            $str[$key] = $value;
            DB::table('form_submissions')->insert(
                [
                    'name' =>  $value[0],
                    'value' => $value[1],
                    'created_at'  => $currentTime,
                    'updated_at'  => $currentTime
                ]
            );
        }
        return DB::table('form_submissions')->get();
    }


    public function fieldFormSubmit(Request $request)
    {
        if (!isset($request) || empty($request)) {
            return;
        }
        $currentTime = date('Y-m-d H:i:s');
        $label = (isset($request->label) && !empty($request->label)) ? $request->label : '';
        $name = (isset($request->name) && !empty($request->name)) ? $request->name : '';
        $required = (isset($request->required) && !empty($request->required)) ? $request->required : 0;
        $id = DB::table('fields')->insertGetId(
            [
                'type'        => $request->type,
                'label'       => $label,
                'name'        => $name,
                'validation'  => $required,
                'created_at'  => $currentTime,
                'updated_at'  => $currentTime
            ]
        );

        if ($id) {
            $result  = DB::table('fields')->where('id', $id)->first();
            return $result;
        }
    }

    public function getAllFields(Request $request)
    {
        return DB::table('fields')->get();
    }
    public function getAllSubmittedFields(Request $request)
    {
        return DB::table('form_submissions')->get();
    }
}
