<?php


use Illuminate\Support\Facades\Route;

use App\Http\Controllers\FormController;

Route::middleware(['cors'])->group(function () {
    Route::post('submitForm', 'FormController@submit');
    Route::post('fieldFormSubmit', 'FormController@fieldFormSubmit');
    Route::get('getAllFields', 'FormController@getAllFields');
    Route::get('getAllSubmittedFields', 'FormController@getAllSubmittedFields');
});
