# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Form test for Evosite

### Setting up the backend: ###

Clone the repository:
git clone https://AWWebdesign@bitbucket.org/AWWebdesign/test.git

Run:
cd test/backend

Run:
npm install

Run:
composer install

Configure the .env file database details in the laravel project, for example:
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=8889
DB_DATABASE=test
DB_USERNAME=root
DB_PASSWORD=root


Run:
php artisan key:generate

Run:
php artisan migrate

Run Mamp in the backend/public folder to start the Laravel project.

Now the backend should be up and running!



### Setting up the frontend: ###

Run (from the route folder):
cd test/frontend

Run:
npm install

Run:
ng serve

In the browser open:
http://localhost:4200/

That’s it! Happy form filling! :)

