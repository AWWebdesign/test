import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  private baseUrl = 'http://localhost:8888/api';

  constructor(
    private http: HttpClient
  ) { }

  submit(data) {
    return this.http.post(`${this.baseUrl}/submitForm`, data);
  }

  fieldFormSubmit(data) {
    return this.http.post(`${this.baseUrl}/fieldFormSubmit`, data);
  }

  getAllFields() {
    return this.http.get(`${this.baseUrl}/getAllFields`);
  }
  getAllSubmittedFields() {
    return this.http.get(`${this.baseUrl}/getAllSubmittedFields`);
  }

}
