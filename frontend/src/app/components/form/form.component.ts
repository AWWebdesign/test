import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { FormService } from 'services/form.service';
import { ValidatorService } from 'services/validator.service';
import { isUndefined } from 'util';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly formService: FormService) { }


  formFieldData$: BehaviorSubject<any> = new BehaviorSubject(undefined);
  submittedFieldsData$: BehaviorSubject<any> = new BehaviorSubject(undefined);

  /**
  * Create the form group
  */
  testForm: FormGroup = this.formBuilder.group({
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    email: ['', [Validators.required, ValidatorService.emailValidator]],
    gender: ['', [Validators.required]],
    age: ['', [Validators.required]],
    message: [''],
    newsletter: [''],
    service: [''],
  });

  fieldForm: FormGroup = this.formBuilder.group({
    type: ['', [Validators.required]],
    name: ['', [Validators.required]],
    label: ['', [Validators.required]],
    required: ['']
  });

  customForm: FormGroup = this.formBuilder.group({});

  formValues: FormData = undefined;
  fieldFormValues: FormFieldData = undefined;

  onSubmit() {
    if (this.customForm.dirty && this.customForm.valid) {

      let formdata = this.customForm.value;

      let dataArray = [];
      for (const [key, value] of Object.entries(formdata)) {
        dataArray.push([key, value])
      }
      this.formService.submit(dataArray).subscribe(
        data => {
          this.renderSubmittedFields(data);
        },
        error => console.log(error)
      );
    }

  }

  onFieldSubmit() {
    if (this.fieldForm.dirty && this.fieldForm.valid) {

      if (!this.fieldFormValues) {
        return;
      }
      this.formService.fieldFormSubmit(this.fieldFormValues).subscribe(
        data => {
          let originalResults = this.formFieldData$.getValue();
          if (!originalResults) {
            return;
          }
          let newResults = [];
          originalResults.forEach(item => {
            newResults.push(item)
          });
          newResults.push(data);
          this.renderForm(newResults);
          this.fieldForm.reset();
        },
        error => console.log(error)
      );
    }

  }


  ngOnInit() {
    this.fieldForm.valueChanges.subscribe(formValues => {
      this.fieldFormValues = formValues;
    });

    this.fieldForm.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(200)).subscribe(formValues => {
        let fieldName = this.lettersOnly(this.fieldForm.controls.label.value);
        this.fieldForm.controls.name.setValue(fieldName);
        this.formValues = formValues;
      });

    this.formService.getAllFields().subscribe(result => {
      this.renderForm(result);
    });

    this.formService.getAllSubmittedFields().subscribe(result => {
      this.renderSubmittedFields(result);
    });

  }

  renderForm(result) {
    this.customForm = this.toFormGroup(result);
    this.formFieldData$.next(result);
  }
  renderSubmittedFields(result) {
    this.submittedFieldsData$.next(result);
  }


  toFormGroup(data) {
    const group: any = {};
    for (const key in data) {
      let validation = null;
      if (data[key].validation === 1) {
        validation = Validators.required;
      }
      if (data[key].type === "email") {
        validation = ValidatorService.emailValidator;

      }
      group[data[key].name] = new FormControl('', validation);
    };

    return new FormGroup(group);
  }

  lettersOnly(str) {
    if (!str) {
      return;
    }
    return str.replace(/[^0-9a-z]/gi, "");
  }



}

export interface FormData {
  firstName: string;
  lastName: string;
  email: string;
  gender: string;
  age: string;
  message: string;
  newsletter: string;
  service: string;
}

export interface FormFieldData {
  id: string;
  type: string;
  label: string;
  name: string;
}

